# Retro Test Data

Test data for Libretro based emulators.

This is a collection of test programs for various emualtors, put here so they
are more easily and more reliably accessible to the GNOME Games CI tests without
cluttering its Git repository.

## 240p Test Suite

Files in the `testsuite240p` directory come from the
[240p Test Suite](http://junkerhq.net/240p/) project. They are mirrored here
because they are normally hosted on
[SourceForge](https://sourceforge.net/projects/testsuite240p/) and downloads
from there fail way too often.

The content of the `testsuite240p` directory is licensed under GPL-2.0-only.

## Open GameBoy Games

Files in the `opengbgames` directory are mirrored here because they are normally
hosted on
[SourceForge](https://sourceforge.net/projects/opengbgames/) and downloads from
there fail way too often.

The content of the `opengbgames` directory is licensed under GPL-2.0-only.

